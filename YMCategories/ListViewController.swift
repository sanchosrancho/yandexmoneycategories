//
//  ListViewController.swift
//  YMCategories
//
//  Created by Alex Shevlyakov on 18/08/15.
//  Copyright (c) 2015 Alex Shevlyakov. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

enum DataLoadingState {
    case Loading
    case Done
//    case Error
}

class ListViewController: UITableViewController {
    
    var categoryId: Int?
    var categories: [Category] {
        return categoryId == nil ? Categories.rootLevel() : Categories.findSubsBy(categoryId!)
    }
    var notificationBlock: NotificationToken?
    var state: DataLoadingState = .Done {
        didSet {
            switch state {
            case .Done:
                self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Refresh, target: self, action: "refresh")
            case .Loading:
                let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
                self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: activityIndicator)
                activityIndicator.startAnimating()
            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "CategoryCellIdentifier")
        
        if let category = Categories.findBy(categoryId) {
            self.title = category.title
        } else {
            self.title = "Categories".localized()
            self.state = .Done
        }
    }
    
    func refresh() {
        self.state = .Loading
        Categories.loadList({
                self.state = .Done
            }, errorBlock: { error in
                self.state = .Done
                println("Some error occured during loading the categories: \(error.localizedDescription)")
            })
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
        notificationBlock = Realm().addNotificationBlock { notification, realm in
            self.tableView.reloadData()
        }
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        if let notificationBlockUnwrapped = notificationBlock {
            Realm().removeNotification(notificationBlockUnwrapped)
        }
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Int(categories.count)
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CategoryCellIdentifier", forIndexPath: indexPath) as! UITableViewCell
        let cat = categories[indexPath.row]
        cell.textLabel!.text = cat.title
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if categories.count > indexPath.row {
            let cat = categories[indexPath.row]
            
            if cat.hasSubs {
                let viewController = self.storyboard?.instantiateViewControllerWithIdentifier("ListViewController") as! ListViewController
                viewController.categoryId = cat.id
                self.navigationController?.pushViewController(viewController, animated: true)
            } else {
                let viewController = self.storyboard?.instantiateViewControllerWithIdentifier("DetailViewController") as! DetailViewController
                viewController.titleText = cat.title
                self.navigationController?.pushViewController(viewController, animated: true)
            }
            
        }
    }
    
}