//
//  DetailViewController.swift
//  YMCategories
//
//  Created by Alex Shevlyakov on 20/08/15.
//  Copyright (c) 2015 Alex Shevlyakov. All rights reserved.
//

import Foundation
import UIKit

class DetailViewController: UIViewController {
    
    var titleText = "" {
        didSet {
            if titleLabel != nil {
                titleLabel.text = titleText
                self.title = titleText
            }
        }
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = titleText
        self.title = titleText
    }
    
}