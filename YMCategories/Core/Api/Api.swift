//
//  Api.swift
//  YMCategories
//
//  Created by Alex Shevlyakov on 19/08/15.
//  Copyright (c) 2015 Alex Shevlyakov. All rights reserved.
//

import Foundation

struct Api {
    static func category() -> ApiCategory {
        return ApiCategory()
    }
}

struct ApiCategory {
    func list() -> ApiRequest {
        return ApiRequest(methodName: "categories-list")
    }
}
