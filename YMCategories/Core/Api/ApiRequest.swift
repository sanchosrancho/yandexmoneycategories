//
//  ApiRequest.swift
//  YMCategories
//
//  Created by Alex Shevlyakov on 19/08/15.
//  Copyright (c) 2015 Alex Shevlyakov. All rights reserved.
//

import Foundation
import Alamofire

struct ApiRequest {
    
    static let URL    = "https://money.yandex.ru/api/"
    static let Domain = NSURL(string: URL)!.host!
    
    var methodName = ""
    var methodType = Alamofire.Method.GET
    
    var completeBlock:(AnyObject -> Void)!
    var errorBlock:((NSError!) -> Void)!
    
    init(methodName: String) {
        self.methodName = methodName
    }
    
    mutating func executeWithComplete(complete: (AnyObject -> Void), error: (NSError! -> Void)) {
        self.completeBlock = complete
        self.errorBlock = error
        
        if self.methodName.isEmpty == true {
            self.errorBlock(ApiRequest.errorWithMessage("Empty method name"))
            return
        }
        
        sendRequest()
    }
    
    private func sendRequest() {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        let requestUrl = ApiRequest.URL + self.methodName
        Alamofire.request(self.methodType, requestUrl, parameters: nil, encoding: ParameterEncoding.JSON)
            .responseJSON { request, response, data, error in
                if let err = error {
                    self.errorBlock(error)
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                    return
                }
                
                if let json = data {
                    self.completeBlock(json)
                } else {
                    self.errorBlock(ApiRequest.errorWithMessage("Response error"))
                }
                
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        }
    }
    
    static func errorWithMessage(msg: String, response: NSDictionary = [:]) -> NSError {
        let unwrappedOptional = response
        
        let userInfo = [
            NSLocalizedDescriptionKey: NSLocalizedString(msg, comment: ""),
            NSLocalizedRecoverySuggestionErrorKey: NSLocalizedString("Contact to admin or developers", comment: ""),
            "response": response
        ]
        return NSError(domain: ApiRequest.Domain, code: 0, userInfo: userInfo)
    }
}