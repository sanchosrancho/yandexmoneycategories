//
//  Categories.swift
//  YMCategories
//
//  Created by Alex Shevlyakov on 19/08/15.
//  Copyright (c) 2015 Alex Shevlyakov. All rights reserved.
//

import Foundation
import RealmSwift

struct Categories {
    
    static func loadList(completeBlock: (Void -> Void), errorBlock: (NSError! -> Void)) {
        var request = Api.category().list()
        request.executeWithComplete(
            { json in
                let realm = Realm()
                realm.write {
                    
                    let updateTimestamp = NSDate().timeIntervalSince1970
                    if let cats = json as? [NSDictionary] {
                        for cat in cats {
                            self.parseCategory(cat, updatedAt: updateTimestamp)
                        }
                    }
                    
                    let categoriesToBeRemoved = realm.objects(Category).filter("updatedAt != %lf", updateTimestamp)
                    realm.delete(categoriesToBeRemoved)
                }
                
                completeBlock()
            },
            error: { error in
                errorBlock(error)
            }
        )
    }
    
    static private func parseCategory(json: NSDictionary, updatedAt: NSTimeInterval, parent: Category? = nil) {
        if let id = json["id"] as? Int?, title = json["title"] as? String {
            var category = Category()
            category.id = (id != nil) ? id! : title.hashValue
            category.title = title
            category.updatedAt = updatedAt
            
            if parent != nil {
                category.parent = parent
            }
            
            if let cats = json["subs"] as? [NSDictionary] {
                for cat in cats {
                    self.parseCategory(cat, updatedAt: updatedAt, parent: category)
                }
            }
            Realm().add(category, update: true)
        }
    }
    
    static func all() -> [Category] {
        return Realm().objects(Category).toArray(Category.self) as [Category]
    }
    
    static func rootLevel() -> [Category] {
        return Realm().objects(Category).filter("parent == nil").toArray(Category.self) as [Category]
    }
    
    static func findBy(id: Int?) -> Category? {
        if id == nil { return nil }
        return Realm().objectForPrimaryKey(Category.self, key: id!)
    }
    
    static func findSubsBy(parentId: Int) -> [Category] {
        if let parent = self.findBy(parentId) {
            return parent.subs
        } else {
            return []
        }
    }
    
    static func removeAll() {
        let realm = Realm()
        realm.write {
            realm.delete(realm.objects(Category))
        }
    }
    
}