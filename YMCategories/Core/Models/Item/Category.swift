//
//  Category.swift
//  YMCategories
//
//  Created by Alex Shevlyakov on 19/08/15.
//  Copyright (c) 2015 Alex Shevlyakov. All rights reserved.
//

import Foundation
import RealmSwift

class Category: Object {
    
    dynamic var id: Int = 0
    dynamic var title = ""
    dynamic var updatedAt: NSTimeInterval = 0.0
    
    // relations
    dynamic var parent: Category?
    var subs: [Category] {
        return linkingObjects(Category.self, forProperty: "parent")
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    var hasSubs: Bool {
        return self.subs.count > 0
    }
}