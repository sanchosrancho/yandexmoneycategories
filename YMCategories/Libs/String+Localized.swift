//
//  String+Localized.swift
//  YMCategories
//
//  Created by Alex Shevlyakov on 19/08/15.
//  Copyright (c) 2015 Alex Shevlyakov. All rights reserved.
//

import Foundation

extension String {
    func localized(comment:String = "") -> String {
        return NSLocalizedString(self, tableName: nil, bundle: NSBundle.mainBundle(), value: "", comment: comment)
    }
}