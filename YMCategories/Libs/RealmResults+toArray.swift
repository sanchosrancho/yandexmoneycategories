//
//  RealmResults+toArray.swift
//  YMCategories
//
//  Created by Alex Shevlyakov on 19/08/15.
//  Copyright (c) 2015 Alex Shevlyakov. All rights reserved.
//

import RealmSwift

extension Results {
    func toArray<T>(ofType: T.Type) -> [T] {
        var array = [T]()
        for var i = 0; i < count; i++ {
            if let result = self[i] as? T {
                array.append(result)
            }
        }
        
        return array
    }
}